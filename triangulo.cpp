#include <stdio.h>
#include <stdlib.h>

int main () {
    int ld1, ld2, ld3;
    printf("Digite o primeiro lado do triangulo: ");
    scanf("%d", &ld1);
    printf("Digite o segundo lado do triangulo: ");
    scanf("%d", &ld2);
    printf("Digite o terceiro lado do triangulo: ");
    scanf("%d", &ld3);

    if((ld1 < ld2 + ld3) && (ld2 < ld1 + ld3) && (ld3 < ld1 + ld2)) {
        if((ld1 == ld2) && (ld2 == ld3) && (ld1 == ld3)) {
            printf("O triangulo eh equilatero!");
        }
        else if((ld1 != ld2) && (ld2 != ld3) && (ld1 != ld3)) {
            printf("O triangulo eh escaleno!");
        }
        else {
            printf("O triangulo eh isosceles!");
        }
    }
    else {
        printf("Um triangulo com tais lados nao pode existir.");
    }
    return 0;
}
