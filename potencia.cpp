#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main () {
	int gols1, gols2;
	float porc;
	printf("Quantidade de gols do Uniao do Dragao Z: ");
	scanf("%d", &gols1);
	printf("Quantidade total de gols no campeonato: ");
	scanf("%d", &gols2);
	if((gols1 < 0,) || (gols2 < 0)) {
		printf("Erro! Voce inseriu um valor negativo!");
	}
	else {
	porc = (100*gols1)/gols2;
	printf("A porcentagem de gols do Uniao do Dragao Z foi de %.2f", porc);
	}
	return 0;
}
